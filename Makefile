IMAGE=bashofmann/test-server
TAG=dev

build:
	docker build -t $(IMAGE):$(TAG) .

push: build
	docker push $(IMAGE):$(TAG)

run: build
	docker run --name test-server --net=host --rm -p 8000:8000 $(IMAGE):$(TAG)

deploy: push
	helm upgrade --install test-server ./chart/test-server --namespace test --create-namespace --set image.tag=$(TAG)
